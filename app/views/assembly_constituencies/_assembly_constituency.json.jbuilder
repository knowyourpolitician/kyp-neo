json.extract! assembly_constituency, :id, :created_at, :updated_at
json.url assembly_constituency_url(assembly_constituency, format: :json)
