json.extract! loksabha_constituency, :id, :created_at, :updated_at
json.url loksabha_constituency_url(loksabha_constituency, format: :json)
