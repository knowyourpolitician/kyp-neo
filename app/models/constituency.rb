class Constituency
  include Mongoid::Document

  field :constituency_name, type: String
  field :constituency_id, type: String
  field :geo_location, type: String
  field :pin_code, type: String
  field :reservation, type: String
  field :type, type: String

  has_and_belongs_to_many :politicians, inverse_of :politician
  belongs_to :state 
  
end
