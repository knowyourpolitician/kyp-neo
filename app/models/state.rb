class State
  include Mongoid::Document

  field :state_name, type: String
  field :state_id, type: String
  field :cheif_minister, type: String
  
  has_many :ministers, inverse_of :politician
  has_many :loksabha_constituencies, inverse_of :constituency
  has_many :assembly_constituencies, inverse_of :constituency
  has_many :elections, inverse_of :election
  
end
