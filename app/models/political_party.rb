class Politicalparty
  include Mongoid::Document
  field :party_name, type: String
  field :party_symbol, type: String
  field :extra, type: String
  
  has_and_belongs_to_many :politicians, inverse_of :politician
end
