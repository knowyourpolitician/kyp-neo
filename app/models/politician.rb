class Politician
  include Mongoid::Document
  
  field :politician_name, type: String
  field :politician_id, type: String
  field :gender, type: String
  field :date_of_birth, type: String
  field :qualifications, type: Array
  field :assets, type: Array
  field :sns_profiles, type: Array
  field :mugshot, type: String
  field :extra, type: String

  belongs_to :constituency
  belongs_to :politicalparty
  # age should be calculated
end
