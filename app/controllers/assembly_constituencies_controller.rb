class AssemblyConstituenciesController < ApplicationController
  before_action :set_assembly_constituency, only: [:show, :edit, :update, :destroy]

  # GET /assembly_constituencies
  # GET /assembly_constituencies.json
  def index
    @assembly_constituencies = AssemblyConstituency.all
  end

  # GET /assembly_constituencies/1
  # GET /assembly_constituencies/1.json
  def show
  end

  # GET /assembly_constituencies/new
  def new
    @assembly_constituency = AssemblyConstituency.new
  end

  # GET /assembly_constituencies/1/edit
  def edit
  end

  # POST /assembly_constituencies
  # POST /assembly_constituencies.json
  def create
    @assembly_constituency = AssemblyConstituency.new(assembly_constituency_params)

    respond_to do |format|
      if @assembly_constituency.save
        format.html { redirect_to @assembly_constituency, notice: 'Assembly constituency was successfully created.' }
        format.json { render :show, status: :created, location: @assembly_constituency }
      else
        format.html { render :new }
        format.json { render json: @assembly_constituency.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assembly_constituencies/1
  # PATCH/PUT /assembly_constituencies/1.json
  def update
    respond_to do |format|
      if @assembly_constituency.update(assembly_constituency_params)
        format.html { redirect_to @assembly_constituency, notice: 'Assembly constituency was successfully updated.' }
        format.json { render :show, status: :ok, location: @assembly_constituency }
      else
        format.html { render :edit }
        format.json { render json: @assembly_constituency.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assembly_constituencies/1
  # DELETE /assembly_constituencies/1.json
  def destroy
    @assembly_constituency.destroy
    respond_to do |format|
      format.html { redirect_to assembly_constituencies_url, notice: 'Assembly constituency was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assembly_constituency
      @assembly_constituency = AssemblyConstituency.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assembly_constituency_params
      params.fetch(:assembly_constituency, {})
    end
end
