class LoksabhaConstituenciesController < ApplicationController
  before_action :set_loksabha_constituency, only: [:show, :edit, :update, :destroy]

  # GET /loksabha_constituencies
  # GET /loksabha_constituencies.json
  def index
    @loksabha_constituencies = LoksabhaConstituency.all
  end

  # GET /loksabha_constituencies/1
  # GET /loksabha_constituencies/1.json
  def show
  end

  # GET /loksabha_constituencies/new
  def new
    @loksabha_constituency = LoksabhaConstituency.new
  end

  # GET /loksabha_constituencies/1/edit
  def edit
  end

  # POST /loksabha_constituencies
  # POST /loksabha_constituencies.json
  def create
    @loksabha_constituency = LoksabhaConstituency.new(loksabha_constituency_params)

    respond_to do |format|
      if @loksabha_constituency.save
        format.html { redirect_to @loksabha_constituency, notice: 'Loksabha constituency was successfully created.' }
        format.json { render :show, status: :created, location: @loksabha_constituency }
      else
        format.html { render :new }
        format.json { render json: @loksabha_constituency.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /loksabha_constituencies/1
  # PATCH/PUT /loksabha_constituencies/1.json
  def update
    respond_to do |format|
      if @loksabha_constituency.update(loksabha_constituency_params)
        format.html { redirect_to @loksabha_constituency, notice: 'Loksabha constituency was successfully updated.' }
        format.json { render :show, status: :ok, location: @loksabha_constituency }
      else
        format.html { render :edit }
        format.json { render json: @loksabha_constituency.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loksabha_constituencies/1
  # DELETE /loksabha_constituencies/1.json
  def destroy
    @loksabha_constituency.destroy
    respond_to do |format|
      format.html { redirect_to loksabha_constituencies_url, notice: 'Loksabha constituency was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loksabha_constituency
      @loksabha_constituency = LoksabhaConstituency.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def loksabha_constituency_params
      params.fetch(:loksabha_constituency, {})
    end
end
