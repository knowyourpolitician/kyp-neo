Rails.application.routes.draw do
  resources :politicians
  resources :political_parties
  resources :states
  resources :assembly_constituencies
  resources :loksabha_constituencies
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
