require 'test_helper'

class AssemblyConstituenciesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assembly_constituency = assembly_constituencies(:one)
  end

  test "should get index" do
    get assembly_constituencies_url
    assert_response :success
  end

  test "should get new" do
    get new_assembly_constituency_url
    assert_response :success
  end

  test "should create assembly_constituency" do
    assert_difference('AssemblyConstituency.count') do
      post assembly_constituencies_url, params: { assembly_constituency: {  } }
    end

    assert_redirected_to assembly_constituency_url(AssemblyConstituency.last)
  end

  test "should show assembly_constituency" do
    get assembly_constituency_url(@assembly_constituency)
    assert_response :success
  end

  test "should get edit" do
    get edit_assembly_constituency_url(@assembly_constituency)
    assert_response :success
  end

  test "should update assembly_constituency" do
    patch assembly_constituency_url(@assembly_constituency), params: { assembly_constituency: {  } }
    assert_redirected_to assembly_constituency_url(@assembly_constituency)
  end

  test "should destroy assembly_constituency" do
    assert_difference('AssemblyConstituency.count', -1) do
      delete assembly_constituency_url(@assembly_constituency)
    end

    assert_redirected_to assembly_constituencies_url
  end
end
