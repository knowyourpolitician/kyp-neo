require 'test_helper'

class LoksabhaConstituenciesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @loksabha_constituency = loksabha_constituencies(:one)
  end

  test "should get index" do
    get loksabha_constituencies_url
    assert_response :success
  end

  test "should get new" do
    get new_loksabha_constituency_url
    assert_response :success
  end

  test "should create loksabha_constituency" do
    assert_difference('LoksabhaConstituency.count') do
      post loksabha_constituencies_url, params: { loksabha_constituency: {  } }
    end

    assert_redirected_to loksabha_constituency_url(LoksabhaConstituency.last)
  end

  test "should show loksabha_constituency" do
    get loksabha_constituency_url(@loksabha_constituency)
    assert_response :success
  end

  test "should get edit" do
    get edit_loksabha_constituency_url(@loksabha_constituency)
    assert_response :success
  end

  test "should update loksabha_constituency" do
    patch loksabha_constituency_url(@loksabha_constituency), params: { loksabha_constituency: {  } }
    assert_redirected_to loksabha_constituency_url(@loksabha_constituency)
  end

  test "should destroy loksabha_constituency" do
    assert_difference('LoksabhaConstituency.count', -1) do
      delete loksabha_constituency_url(@loksabha_constituency)
    end

    assert_redirected_to loksabha_constituencies_url
  end
end
